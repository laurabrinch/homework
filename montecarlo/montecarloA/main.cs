using System;
using static System.Console;
using static System.Math;

public class main{
	public static void Main(){

	WriteLine($"Calculating some two-dimensional integrals");
		Func<vector,double> f1=x=> x[0]+Sin(x[1])+1;
		vector a1 = new vector(0,-PI);
		vector b1 = new vector(2,PI);
		var result1=montecarlo.plainmc(f1,a1,b1,1000);
		
		Func<vector,double> f2=x=> x[0]*x[1];
		vector a2 = new vector(0,0);
		vector b2 = new vector(1,1);
		var result2=montecarlo.plainmc(f2,a2,b2,1000);


	WriteLine($"Integral of x+sin(y)+1 from 0 to 2, and from -pi to pi = {result1.Item1} and the error is {result1.Item2}");
	WriteLine($"Integral of x*y from 0 to 1 and from 0 to 1 = {result2.Item1} and the error is {result2.Item2}");

	WriteLine($"Trying to calculate the integral of [1-cos(x)*cos(y)*cos(z)]^-1 from 0 to pi, from 0 to pi and from 0 to pi");
		Func<vector,double> f3=x=> 1/(1-Cos(x[0])*Cos(x[1])*Cos(x[2]))*1/(PI*PI*PI);
		vector a3 = new vector(0,0,0);
		vector b3 = new vector(PI,PI,PI);
		var result3=montecarlo.plainmc(f3,a3,b3,1000);

	WriteLine($"The answer is {result3.Item1} and the error is {result3.Item2}");
	}
}
