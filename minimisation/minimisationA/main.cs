using System; 
using static System.Console; 
using static System.Math; 
public class main{
	public static void Main(){
        WriteLine($"Finding the minimum of the Rosenbrock's valley function f(x,y)=(1-x)^2+100(y-x^2)^2");
		Func<vector,double> f1=x=> Pow((1-x[0]),2)+100*Pow((x[1]-Pow(x[0],2)),2);
		vector x01 = new vector(0,0);
        double acc1 = 1e-4;
        var (res1,steps1) = minimisation.qnewton(f1,x01,acc:acc1);
		WriteLine($"My result is x={res1[0]}, y={res1[1]} with {steps1} steps, and the actual minimun is at x=1, y=1");

        WriteLine($"Finding the minimum of the Himmelblau's function f(x,y)=(x^2+y-11)^2+(x+y^2-7)^2");
		Func<vector,double> f2=x=> Pow((Pow(x[0],2)+x[1]-11),2)+Pow((x[0]+Pow(x[1],2)-7),2);
		vector x02 = new vector(1,1);
        var (res2,steps2) = minimisation.qnewton(f2,x02,acc:acc1);
		WriteLine($"My result is x={res2[0]}, y={res2[1]} with {steps2} steps, and the actual minimun is at x=3, y=2");


    }
}