using System; 
using static System.Console; 
using static System.Math; 
using static System.Random; //generates random numbers

public class main{
	public static void Main(){
		int n = 8; //making matrices
		int m = 5;
		matrix A = new matrix(n,m);
		matrix R = new matrix(m,m);
		matrix Q = new matrix(n,m);

		var rand = new Random(); //Random numbers for matrices

		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				double random = rand.NextDouble();
				A[i,j] = random; //returns a double between 0 and 1
				Q[i,j] = random; 
			}
		}
		WriteLine("Random matrix A:");
		A.print();

		WriteLine("Factorizing it into QR:");
		lineq.QRGSdecomp(Q,R);

		WriteLine("Q matrix after decomposition:");
		Q.print();

		WriteLine("R matrix after decomposition:");
		R.print();

		WriteLine("R is upper triangular!");

		WriteLine("Checking that Q^T*Q=1");
		matrix QTQ = Q.transpose()*Q;
		QTQ.print();

		WriteLine("Checking that Q*R=A");
		matrix QR = Q*R;
		QR.print();


		//part 2 of A:
		int n2 = 7;
		int m2 = 7;

		matrix A2 = new matrix(n2,m2);
		matrix R2 = new matrix(n2,m2);
		vector b = new vector(n2);
		for(int i=0; i<n2; i++){
			b[i] = rand.NextDouble();
			for(int j=0; j<m2; j++){
				A2[i,j] = rand.NextDouble();
			}
		}

		WriteLine("Random matrix A2:");
		A2.print();
		WriteLine("Random vector b:");
		b.print();
		matrix A2copy = A2.copy();
		lineq.QRGSdecomp(A2, R2);
		
		WriteLine("Factorizing it into QR:");
		A2.print();
		R2.print();

		WriteLine("Solving Q*R*x=b:");
		vector x = lineq.QRGSsolve(A2,R2,b);
		WriteLine("The solution is:");
		x.print();
		
		WriteLine("Checking that A*x=b");
		vector Ax = A2copy*x;
		WriteLine("A*x=");
		Ax.print();
		WriteLine("b=");
		b.print();
	}
}
