using System; 
using static System.Console; 
using static System.Math; 
using static System.Random; //generates random numbers

public class main{
	public static void Main(){
        int n=5;
        matrix A = new matrix(n,n);
		matrix R = new matrix(n,n);
        matrix Q = new matrix(n,n);
        var rand = new Random(); //Random numbers for matrices

		for(int i=0; i<n; i++){
			for(int j=0; j<n; j++){
				double random = rand.NextDouble();
				A[i,j] = random; //returns a double between 0 and 1
				Q[i,j] = random; 
			}
		}

        WriteLine("Random matrix A:");
		A.print();

		WriteLine("Factorizing it into QR");
		lineq.QRGSdecomp(Q,R);	

        WriteLine("Calculating the inverse B:");
        matrix B = lineq.QRGSinverse(Q,R);
        B.print();

        WriteLine("Checking that AB=I:");
        matrix check = A*B;
        check.print();
        WriteLine("(it almost is)");

    }
}