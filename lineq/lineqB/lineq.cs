using System; 
using static System.Console; 
using static System.Math;

public static class lineq{
	public static void QRGSdecomp(matrix A, matrix R){
		int m = A.size2;
		for(int i=0; i<m; i++){ //this section is from the notes
			R[i,i]=A[i].norm();
			A[i]=A[i]/R[i,i]; 
			for(int j=i+1; j<m; j++){
				R[i,j]=A[i].dot(A[j]);
				A[j]=A[j]-A[i]*R[i,j]; 
			} 
		}
	}

	public static vector QRGSsolve(matrix Q, matrix R, vector b){
		matrix Q_trans = Q.transpose();
		vector x = Q_trans*b;
		backsub(R,x);	
		return x; //this is just like described in part 2 of A
	}

	public static void backsub(matrix U, vector c){
		for(int i=c.size-1; i>=0; i--){
			double sum=0;
			for(int k=i+1; k<c.size; k++){ 
				sum+=U[i,k]*c[k];			
			}
		c[i]=(c[i]-sum)/U[i,i]; //this section is from the notes  
		}
	}


//based on eq 46 in the notes on linear equations    
    public static matrix QRGSinverse(matrix Q, matrix R){
        int n=Q.size1;
        matrix I=new matrix(n,n); //making an identity matrix for e columns
        I.setid();
        matrix B = new matrix(n,n); //this will be the inverse matrix
        for(int i=0; i<n; i++){
            vector e_i=I[i]; //unit vector in the i direction
            vector x=QRGSsolve(Q,R,e_i); //solving the linear equation
            B[i]=x; //putting x into B to construct the inverse matrix
        }
        return B;
    }
}