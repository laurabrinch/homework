using System;
using static System.Console;
using static System.Math;

public class main{
	public static void Main(){
		Func<double,double> f1=x=> Sqrt(x);
		Func<double,double> f2=x=> 1/Sqrt(x);
		Func<double,double> f3=x=> 4*Sqrt(1-Pow(x,2));
		Func<double,double> f4=x=> Log(x)/Sqrt(x);

		double result1 = quadratures.integrate(f1,0,1);
		double result2 = quadratures.integrate(f2,0,1);
		double result3 = quadratures.integrate(f3,0,1);
		double result4 = quadratures.integrate(f4,0,1);

		WriteLine($"Integral of Sqrt(x) from 0 to 1 = {result1}, should give 0.66");
		WriteLine($"Integral of 1/Sqrt(x) from 0 to 1 = {result2}, should give 2");
		WriteLine($"Integral of 4*Sqrt(1-x^2) from 0 to 1 = {result3}, should give 3.14");
		WriteLine($"Integral of ln(x)/Sqrt(x) from 0 to 1 = {result4}, should give -4");

		using(var outfile = new System.IO.StreamWriter("data.txt")){
			for(double x=-2; x<=2; x+=1.0/8){
				outfile.WriteLine($"{x} {quadratures.errorfunc(x)}");
			}
		}
	}
}
