Integral of Sqrt(x) from 0 to 1 = 0.666953909054655, should give 0.66
Integral of 1/Sqrt(x) from 0 to 1 = 1.99996150007805, should give 2
Integral of 4*Sqrt(1-x^2) from 0 to 1 = 3.14183457802727, should give 3.14
Integral of ln(x)/Sqrt(x) from 0 to 1 = -3.99991705899915, should give -4
