using static System.Console;
using static System.Math;

public class main{
        public static void Main(){
                list<int> a = new list<int>();
                a.push(1);
                a.push(2);
                a.push(3);
		WriteLine("Printing using chain of nodes:");
                for( a.start(); a.current != null; a.next()){
                        WriteLine(a.current.item);
                }
	}
}
