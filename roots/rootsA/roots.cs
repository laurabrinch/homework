using System;
using static System.Math;
using static System.Console;

public class roots{
	public static vector newton(Func<vector,vector> f, vector x, double eps=1e-2){
//the following is based on Dmitris notes	
		int n=x.size;
		vector fx=f(x),z,fz;
		while(true){
			matrix J=jacobian(f,x,fx);
			matrix R=new matrix(n,n);
			matrix Q=J.copy();
			lineq.QRGSdecomp(Q,R);
			vector Dx=lineq.QRGSsolve(Q,R,-fx);
			double s=1;
			while(true){// backtracking linesearch, står på side 2 i noten om roots også
				z=x+Dx*s; 
				fz=f(z);
				if(fz.norm()<(1-s/2)*fx.norm()){ break; }
				if(s<1.0/64){ break; }
				s/=2;
			}
			x=z;
			fx=fz;
			if(fx.norm()<eps)break;
		}
		return x;
	}

//creating the jacobian command:

	public static matrix jacobian(Func<vector,vector> f, vector x, vector fx=null, vector dx=null){
		int n=x.size;
//		if(dx==null) dx = x.map(t => Max(Abs(t),1)*Pow(2,-26));
		if(dx==null) dx = x.map(t => Abs(t)*Pow(2,-26));
		matrix J=new matrix(n,n);
		if(fx == null) fx=f(x);
		for(int j=0; j<n; j++){
			x[j]+=dx[j];
			vector df=f(x)-fx;
			for(int i=0; i<n; i++){ 
				J[i,j]=df[i]/dx[j];
			}
			x[j]-=dx[j];
		}
		return J;
	}
}

