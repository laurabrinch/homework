using System; 
using static System.Console; 
using static System.Math; 
using static System.Random; //generates random numbers

public class main{
	public static void Main(){
		WriteLine($"Testing my routine on the function f(x)=log(x)*x^2:");
		Func<vector,vector> f1=x=> new vector(2*x[0]*Log(x[0])+x[0]);
		var x01 = new vector(0.5);
		vector res1 = roots.newton(f1,x01);
		WriteLine($"My result is x={res1[0]}, and the analytical result is 0.6065");


		WriteLine($"Now I can find the extremum of the Rosenbrock's valley function f(x,y)=(1-x)^2+100(y-x^2)^2");
		Func<vector,vector> f2=x=> new vector(2*(200*Pow(x[0],3)-200*x[0]*x[1]+x[0]-1), 200*(x[1]-Pow(x[0],2)));
		var x02 = new vector(0.5,0.5);
		vector res2 = roots.newton(f2,x02);
		WriteLine($"My result is x={res2[0]}, y={res2[1]} and the analytical result is x=1, y=1");
	}
}
