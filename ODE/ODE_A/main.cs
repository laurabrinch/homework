using System;
using static System.Console;
using static System.Math;

public class main{
	public static void Main(){
		double b=0.25; //from scipy manual
		double c=5; //from scipy manual
		Func<double, vector, vector> f=delegate(double x, vector y){
			double theta=y[0];
			double omega=y[1];
			return new vector(omega, -b*omega-c*Sin(theta)); //function from the scipy manual
		}; //don't know why we need ; here, but cant make without it, and Dmitri has it

		double begin=0;
		double end=10;
		vector y0=new vector(PI-0.1,0.0); //from scipy manual
		vector yend=ode.driver(f, begin, y0, end);

	}
}
