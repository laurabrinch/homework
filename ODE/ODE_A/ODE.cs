using System;
using static System.Console;
using static System.Math;

// this is from the homework description
public class ode{

//step part:
	public static (vector,vector) rkstep12(Func<double,vector,vector> f, double x, vector y, double h){    

// Runge-Kutta Euler/Midpoint method (probably not the most effective)

// f is from dy/dx, x is the value of the variable, y is the value of y(x), h is the step to be taken

		vector k0 = f(x,y); // embedded lower order formula (Euler) 
		vector k1 = f(x+h/2,y+k0*(h/2)); // higher order formula (midpoint) 
		vector yh = y+k1*h;     // y(x+h) estimate 
		vector er = (k1-k0)*h;  // error estimate 
		return (yh,er);
	}

//driver part:
	public static vector driver(Func<double,vector,vector> f, double a, vector ya, double b, double h=0.01, double acc=0.01, double eps=0.01){               

//a is the start-point, ya is y(a), b is the end-point, h is initial step size, acc is absolute accuracy goal, 
//eps is relative accuracy goal 

		if(a>b) throw new Exception("driver: a>b");
		double x=a; 
		vector y=ya;
		do {
			if(x>=b) return y; // job done 
			if(x+h>b) h=b-x;   // last step should end at b 

			var (yh,erv) = rkstep12(f,x,y,h);
			double tol = Max(acc,yh.norm()*eps) * Sqrt(h/(b-a));
			double err = erv.norm();
			if(err<=tol){x+=h; y=yh; Error.Write($"{x} {y[0]} {y[1]}\n");} // accept step
			h *= Min( Pow(tol/err,0.25)*0.95 , 2); // reajust stepsize
	        }while(true);
	}//driver
}
