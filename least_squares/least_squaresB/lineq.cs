using System; 
using static System.Console; 
using static System.Math;

public static class lineq{
	public static void QRGSdecomp(matrix A, matrix R){
		int m = A.size2;
		for(int i=0; i<m; i++){ //this section is from the notes
			R[i,i]=A[i].norm();
			A[i]=A[i]/R[i,i]; 
			for(int j=i+1; j<m; j++){
				R[i,j]=A[i].dot(A[j]);
				A[j]=A[j]-A[i]*R[i,j]; 
			} 
		}
	}

	public static vector QRGSsolve(matrix Q, matrix R, vector b){
		matrix Q_trans = Q.transpose();
		vector x = Q_trans*b;
		backsub(R,x);	
		return x; //this is just like described in part 2 of A
	}

	public static void backsub(matrix U, vector c){
		for(int i=c.size-1; i>=0; i--){
			double sum=0;
			for(int k=i+1; k<c.size; k++){ 
				sum+=U[i,k]*c[k];			
			}
		c[i]=(c[i]-sum)/U[i,i]; //this section is from the notes  
		}
	}

//needed for least_squares part B

	public static matrix QRGSinverse(matrix Q, matrix R){
		int m=Q.size1;
		matrix I=new matrix(m,m);
		I.set_identity();

		matrix r=new matrix(m,m);
		for(int i=0; i<m; i++){
			vector inv=I[i];
			vector x=QRGSsolve(Q,R,inv);
			r[i]=x;
		}
		return r;
	}

}