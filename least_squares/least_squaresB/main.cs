using System;
using static System.Console;
using static System.Math;

public class main{
	public static void Main(){
		//alternativt kunne man have skrevet double[] xs = new double[] {tal}, men så skulle jeg ændre 
		// argument i least_squares.cs fra vector til double
		vector xs = new vector(new double[] {1,2,3,4,6,9,10,13,15}); //Time 
		vector ys = new vector(new double[] {117,100,88,72,53,29.5,25.2,15.2,11.1}); //Activity
		vector dys = new vector(new double[] {5,5,5,5,5,5,1,1,1,1}); //Uncertainty

		var fs = new Func<double,double>[] {z=>1.0, z=>z};
		
		WriteLine($"Time:  Activity: Uncertainty:");
		for(int i=0; i<xs.size; i++){
			WriteLine($"  {xs[i]}     {ys[i]}       {dys[i]}");
		}

		for(int i=0; i<ys.size; i++){
			dys[i] = dys[i]/ys[i]; //uncertainty of logarithm, står i opgaven 
			ys[i] = Log(ys[i]);
		}

		(vector cs, matrix cov) = least_squares.fit(xs,ys,dys,fs);
		
		
        double lambda = cs[1];

		double T = -Log(2.0)/lambda;

		using(var outfile = new System.IO.StreamWriter("fit.txt")){
			for(double k=0; k<16; k+=1.0/32){ //Makes function from cs
				double fitfunc = 0;
				for(int j=0; j<fs.Length; j++){
					fitfunc += cs[j]*fs[j](k);
				}
				outfile.WriteLine($"{k} {Exp(fitfunc)}");
			}
		}
        cov.print("The covariance matrix is: ");
        WriteLine($"lambda = {cs[1]} +- {Sqrt(cov[1,1])}");
		WriteLine($"Half life from fit is {T} +- {Sqrt(cov[1,1])*T} days. The real value is 3.6 days, and is not within our uncertainty");

	}
}