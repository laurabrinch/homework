using System;
using static System.Console;
using static System.Math;

public class least_squares{
    public static (vector, matrix) fit(vector x, vector y, vector dy, Func<double,double>[] fs){
        int n = x.size;
        int m = fs.Length;
        matrix A = new matrix(n,m);
        matrix Q = A.copy(); //Q er n x m matrix
		matrix R = new matrix(m,m); //R er altid kvadratisk
		vector b = new vector(n);
		for(int i=0; i<n; i++){
			b[i]=y[i]/dy[i];
			for(int k=0; k<m; k++){ 
				Q[i,k]=fs[k](x[i])/dy[i];
			}
		} //from the pdf

        lineq.QRGSdecomp(Q,R);
		vector c = lineq.QRGSsolve(Q,R,b);
		

        matrix R_trans_times_R = R.transpose()*R;
		matrix R2 = new matrix(R_trans_times_R.size2, R_trans_times_R.size2);
		lineq.QRGSdecomp(R_trans_times_R, R2);
		matrix S = lineq.QRGSinverse(R_trans_times_R, R2);

        return (c,S);

    }

    //public static matrix cov(matrix R_trans_times_R,R2){
    //    matrix R_trans_times_R = R.transpose()*R;
	//	matrix R2 = new matrix(R_trans_times_R.size2, R_trans__times_R.size2);
	//	lineq.QRGSdecomp(R_trans__times_R, R2);
	//	matrix Cov = lineq.QRGSinverse(R_trans_times_R, R2);
    //    return Cov
    //}

}