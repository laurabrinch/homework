using System; 
using static System.Console; 
using static System.Math; 

public class least_squares{
	//public static double[]
	public static double[] fit(vector x, vector y, vector dy, Func<double,double>[] fs){
		int n = x.size;
		int m = fs.Length;
		matrix A = new matrix(n,m);
		matrix Q = A.copy(); //Q er n x m matrix
		matrix R = new matrix(m,m); //R er altid kvadratisk
		vector b = new vector(n);
		for(int i=0; i<n; i++){
			b[i]=y[i]/dy[i];
			for(int k=0; k<m; k++){ 
				Q[i,k]=fs[k](x[i])/dy[i];
			}
		} //from the pdf
		
		lineq.QRGSdecomp(Q,R);
		vector c = lineq.QRGSsolve(Q,R,b);
		return c;
	}

//	public double eval(double x){
//		double fitfunc = 0;
//		for(int i=0; i<fs.Length; i++){
//			fitfunction += c[i]*fs[i](x);
//		} 
//		return fitfunc;
//	}
}





