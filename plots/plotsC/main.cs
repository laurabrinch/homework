using System;
using static System.Console;
using static System.Math;
using static cmath;

class main{
	static complex gamma(complex x){
		complex lngamma=x*log(x+1/(12*x-1/x/10))-x+log(2*PI/x)/2;
		return exp(lngamma);
	}

	public static void Main(){	
		for(double x=-5; x<=5; x+=1.0/10){
			for(double y=-5; y<=5; y+=1.0/10){
				complex a=new complex(x,y);
				complex b=gamma(a);
				WriteLine($"{abs(b)} {x} {y}");
			}
		}
	}
}
