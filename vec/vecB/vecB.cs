using static System.Console;
using static System.Math;

public class vec{
	public double x,y,z;

	//constructors:
	public vec(){x=y=z=0;}
	public vec(double a,double b,double c){x=a;y=b;z=c;}

	//new operators:
	public static double dot(vec v, vec u){return v.x*u.x + v.y*u.y + v.z*u.z;}
	public static vec cross(vec v, vec u){return new vec(v.y*u.z-v.z*u.y, v.z*u.x-v.x*u.z, v.x*u.y-v.y*u.x);}
	public static double norm(vec v){return Sqrt(v.x*v.x + v.y*v.y + v.z*v.z);}

	//print method:
	public void print(string s){Write(s);WriteLine($"{x} {y} {z}");}
	public void print(){this.print("");}

}
