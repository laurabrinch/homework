using System;
using static System.Console;
using static System.Math;
using static vec;

class main{
	static void Main(){
	WriteLine($"Trying out the new operators:");
	
	vec u = new vec(3,6,9);
	u.print("u=");
		
	vec v = new vec(-2,7,1);
	v.print("v=");

	double d = dot(u,v);
	WriteLine($"dot(u,v)={d}");

	vec c = cross(u,v);
	c.print("cross(u,v)=");

	double n = norm(v);
	WriteLine($"norm(v)={n}");
	}
}
