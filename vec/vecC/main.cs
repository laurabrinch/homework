using System;
using static System.Console;
using static System.Math;
using static vec;

class main{
	static void Main(){
		vec u = new vec(3,6,9);
		u.print("u=");
		
		vec v = new vec(-2,7,1);
		v.print("v=");

		WriteLine($"Testing approx(v,v)");
		Write($"v.approx(v) = {v.approx(v)}\n");
		Write($"approx(v,v) = {approx(v,v)}\n");

		WriteLine($"Testing approx(v,u)");
		Write($"v.approx(u) = {v.approx(u)}\n");
		Write($"approx(v,u) = {approx(v,u)}\n");
	}
}
