using System;
using static System.Console;
using static System.Math;
using static vec;

class main{
	static void Main(){
		WriteLine($"Trying out the operators:");
		
		vec u = new vec(3,6,9);
		u.print("u=");
		
		vec v = new vec(-2,7,1);
		v.print("v=");

		(6*u).print("5*u=");
		var tmp=u*6;
		tmp.print("u*5=");

		(v+u).print("v+u=");

		(v-u).print("v-u=");

		(-u).print("-u=");

		var w=3*u-v;
		w.print("w=");
	}

}
