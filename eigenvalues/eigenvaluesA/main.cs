using System;
using static System.Math;
using static System.Console;
using static System.Random;


public class main{
	public static void Main(){
		int n = 4; //det her er ligesom der blev lavet matricer i lineq homework
		int m = 4;
		matrix A = new matrix(n,m);
		matrix V = new matrix(n,m);
		matrix I = new matrix(n,m);
		I.set_unity();
		var rand = new Random(); //Random numbers for matrices

		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				double random = rand.NextDouble();
				A[i,j] = random; //returns a double between 0 and 1
				A[j,i] = random;
			}
		}

		WriteLine("Random matrix A:");
		A.print();

		WriteLine($"Performimg the eigenvalue-decomposition:");
		matrix D = A.copy();
		jacobi.JacobiCyclic(D,V);	
		D.print("D=");
		V.print("V=");

		WriteLine($"Checking that V^T*A*V=D: {D.approx(V.transpose()*A*V)}");
		WriteLine($"Checking that V*D*V^T=A: {A.approx(V*D*V.transpose())}");
		WriteLine($"Checking that V^T*V=1: {I.approx(V.transpose()*V)}");
		WriteLine($"Checking that V*V^T=1: {I.approx(V*V.transpose())}");

	}
}
