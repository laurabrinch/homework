using System; 
using static System.Console; 
using static System.Math;

public class ann{
	public int n; /* number of hidden neurons */
	public Func<double,double> f; /* activation function */
	public vector p; /* network parameters */

	public ann(int n, Func<double,double> f){this.n=n; this.f=f; this.p=new vector(3*n);}

	public double response(double x){
		double sum=0;
        for(int i=0; i<n; i++){
            double a=p[3*i+0];
            double b=p[3*i+1];
            double w=p[3*i+2];
            sum+=w*f((x-a)/b);
        }
    return sum;    
	}

	public void train(vector x,vector y){
	    Func<vector,double> mismatch=u=>{
		    p=u;
		    double sum=0;
		    for(int k=0; k<x.size; k++){
			    sum+=Pow(response(x[k])-y[k],2);
            }
		    return sum/x.size;
		};
	    var (res,steps)=minimisation.qnewton(mismatch,p);
    }
}