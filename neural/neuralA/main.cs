using System; 
using static System.Console; 
using static System.Math; 

//based on Dmitris code:
public class main{
	public static void Main(){
        Func<double,double> f=(x)=>x*Exp(-Pow(x,2)); //Gaussian wavelet
        Func<double,double> g=(x)=>Cos(5*x-1)*Exp(-Pow(x,2));

    	int n=5;
    	var ann=new ann(n,f);

	    double a=-1,b=1;
	    int nx=20;
	    vector xs=new vector(nx);
    	vector ys=new vector(nx);

    	for(int i=0; i<nx; i++){
    		xs[i]=a+(b-a)*i/(nx-1);
    		ys[i]=g(xs[i]);
       		WriteLine($"{xs[i]} {ys[i]}");
		}

		WriteLine($"");
		WriteLine($"");

    	for(int i=0; i<ann.n; i++){
	    	ann.p[3*i+0]=a+(b-a)*i/(ann.n-1);
	    	ann.p[3*i+1]=1;
	    	ann.p[3*i+2]=1;
	    }

	ann.train(xs,ys);
	
	for(double z=a; z<=b; z+=1.0/64)
		WriteLine($"{z} {ann.response(z)}");
    }
}