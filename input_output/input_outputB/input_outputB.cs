using System;
using System.IO;
using static System.Console;
using static System.Math;

class input_output{
	static public void Main(string[] args){
	WriteLine($"x       Sin(x)           Cos(x)");
	foreach(var arg in args){
		double x = double.Parse(arg);
		WriteLine($"{x} {Sin(x)} {Cos(x)}");
		}
	} 
}
